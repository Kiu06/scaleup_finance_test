import styled from 'styled-components';

const Section = styled.section`
  margin: 3em auto;
  padding: 0 2em;

  &:first-child {
    margin-top: 0;
  }
`;

export default Section;
