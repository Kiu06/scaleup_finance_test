import React from 'react';
import Carousel, { slidesToShowPlugin } from '@brainhubeu/react-carousel';
import '@brainhubeu/react-carousel/lib/style.css';
import SliderItem from 'components/SliderItem';
import slides from './slides';

const MyCarousel = () => (
  <Carousel plugins={['infinite', {
    resolve: slidesToShowPlugin,
    options: {
      numberOfSlides: 3
    }
  },]}>
    {slides.map(item => (
      <SliderItem key={item.slide}>
        <img src={item.image} />
        <div className="icon">
          <img className="icon-img" src={item.icon} />
        </div>
        <h3>{item.title}</h3>
        <p>{item.description}</p>
      </SliderItem>
    ))}
  </Carousel>
);

export default MyCarousel;