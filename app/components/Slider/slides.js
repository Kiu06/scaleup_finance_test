import Surfing from './images/surfing.png';
import SurfingIcon from './icons/icn_surfing.svg';
import Hiking from './images/hiking.png';
import HikingIcon from './icons/icn_hike.png';
import Weights from './images/weights.png';
import WeightsIcon from './icons/icn_weights.svg';
import Spinning from './images/spinning.png';
import SpinningIcon from './icons/icn_spin.svg';

const slides = [
    {
      slide: 'slide1',
      title: 'Surfing',
      description: 'Ocean Beach',
      image: Surfing,
      icon: SurfingIcon,
    },
    {
        slide: 'slide2',
        title: 'Hiking',
        description: 'Torrey Pines',
        image: Hiking,
        icon: HikingIcon,
    },
    {
        slide: 'slide3',
        title: 'Weights',
        description: 'Weights',
        image: Weights,
        icon: WeightsIcon,
    },
    {
        slide: 'slide4',
        title: 'Spinning',
        description: 'Spinning',
        image: Spinning,
        icon: SpinningIcon,
    },
];
  
export default slides;