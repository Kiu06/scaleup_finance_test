import React from 'react';
import H2 from 'components/H2';
import P from 'components/P';
import Button from 'components/Button';

export function Shedule() {
    return (
        <div>
            <H2>Scheduled Activities</H2>
            <P>You don’t have any activities scheduled yet.</P>
            <Button>Schedule activity</Button>
        </div>
    )
}

export default Shedule;