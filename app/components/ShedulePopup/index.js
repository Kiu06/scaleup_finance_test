import React from 'react';
import Wrapper from './Wrapper';
import H2 from 'components/H2';
import P from 'components/P';

export function ShedulePopup() {
    return (
        <Wrapper>
            <H2>Schedule Activities</H2>
            <P>You don’t have any activities scheduled yet.</P>
        </Wrapper>
    )
}

export default ShedulePopup;