import styled from 'styled-components';

const Wrapper = styled.div`
  display: none;
  padding: 3em 0;
  position: fixed;
  top: 0;
  width: calc(768px + 16px * 2);
  height: 100%;
  background: #344856;

  &.active {
    display: block;
  }
`;

export default Wrapper;
