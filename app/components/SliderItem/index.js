import styled from 'styled-components';

export default styled.div`
    margin: 2em 3em;
    box-shadow: 0px 0px 10px #1b1c201a;
    border-radius: 15px;
    position: relative;
    &:hover {
        cursor: pointer;
    }
    img {
        max-width: 100%;
    }
    h3 {
        text-align: center;
        font-family: 'Europa-Bold', sans-serif;
        color: #334856;
        font-size: 15px;
        margin: 2em 0 0;
        line-height: 1;
    }
    p {
        text-align: center;
        font-family: 'Rift', 'Open Sans';
        color: #7D8184;
        font-size: 10px;
        letter-spacing: .3px;
        text-transform: uppercase;
        padding: 0 0 1em;
    }
    .icon {
        position: absolute;
        top: 103px;
        left: 59px;
        background: #fff;
        padding: 7px;
        border-radius: 50%;
        box-shadow: 0px 10px 30px #42596529;
        img {
            max-width: 30px;
        }
    }
`;
