import React from 'react';
import H2 from 'components/H2';
import P from 'components/P';

export function History() {
    return (
        <div>
            <H2>Activity History</H2>
            <P>You haven’t tracked any activities yet.</P>
        </div>
    )
}

export default History;