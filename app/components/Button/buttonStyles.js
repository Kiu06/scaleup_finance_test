import { css } from 'styled-components';

const buttonStyles = css`
  display: inline-block;
  box-sizing: border-box;
  padding: 12px 45px;
  text-decoration: none;
  border-radius: 50px;
  -webkit-font-smoothing: antialiased;
  -webkit-touch-callout: none;
  user-select: none;
  cursor: pointer;
  outline: 0;
  font-family: 'Rift', 'Open Sans', sans-serif;
  font-size: 14px;
  color: #fff;
  background: #D97D54;
  text-transform: uppercase;
  -webkit-transition: all 0.3s;
  -moz-transition: all 0.3s;
  -o-transition: all 0.3s;
  transition: all 0.3s;

  &:hover {
    box-shadow: 0px 0px 10px #1b1c201a;
  }
  img {
    margin-right: 10px;
  }
`;

export default buttonStyles;
