import styled from 'styled-components';

const P = styled.p`
  font-size: 14px;
  font-family: 'Europa Light', 'Open Sans';
  color: #6E8CA0;
`;

export default P;
