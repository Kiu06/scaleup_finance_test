import styled from 'styled-components';

export default styled.div`
  display: flex;
  padding: 2em;
  text-decoration: none;
  -webkit-font-smoothing: antialiased;
  -webkit-touch-callout: none;
  user-select: none;
  cursor: pointer;
  outline: 0;

  img {
    max-width: 115px;
  }
`;