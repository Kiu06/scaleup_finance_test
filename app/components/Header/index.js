import React from 'react';

import Img from './Img';
import NavBar from './NavBar';
import HeaderLogo from './HeaderLogo';
import LogoImg from './logoImg.svg';

function Header() {
  return (
    <div>
      <NavBar>
        <HeaderLogo>
          <Img src={LogoImg} alt="logo" />
        </HeaderLogo>
      </NavBar>
    </div>
  );
}

export default Header;
