import styled from 'styled-components';

export default styled.div`
  text-align: center;
  box-shadow: 0px 0px 10px #1B1C201A;
`;
