import { createGlobalStyle } from 'styled-components';

const GlobalStyle = createGlobalStyle`
  html,
  body {
    height: 100%;
    width: 100%;
    line-height: 1.5;
  }

  @font-face {
    font-family: 'Europa-Light';
    src: url('fonts/europa-light-webfont.eot'); /* IE9 Compat Modes */
    src: url('fonts/europa-light-webfont.eot?#iefix') format('embedded-opentype'), /* IE6-IE8 */
         url('fonts/europa-light-webfont.woff2') format('woff2'), /* Super Modern Browsers */
         url('fonts/europa-light-webfont.woff') format('woff'), /* Pretty Modern Browsers */
         url('fonts/europa-light-webfont.ttf')  format('truetype'), /* Safari, Android, iOS */
         url('fonts/europa-light-webfont.svg#svgFontName') format('svg'); /* Legacy iOS */
  }

  @font-face {
    font-family: 'Europa-Bold';
    src: url('fonts/europa-bold-webfont.eot'); /* IE9 Compat Modes */
    src: url('fonts/europa-bold-webfont.eot?#iefix') format('embedded-opentype'), /* IE6-IE8 */
         url('fonts/europa-bold-webfont.woff2') format('woff2'), /* Super Modern Browsers */
         url('fonts/europa-bold-webfont.woff') format('woff'), /* Pretty Modern Browsers */
         url('fonts/europa-bold-webfont.ttf')  format('truetype'), /* Safari, Android, iOS */
         url('fonts/europa-bold-webfont.svg#svgFontName') format('svg'); /* Legacy iOS */
  }

  @font-face {
    font-family: 'Rift';
    src: url('fonts/rift/Rift-Regular.otf'); /* IE9 Compat Modes */
  }

  body {
    font-family: 'Europa Light', Helvetica, Arial, sans-serif;
  }

  body.fontLoaded {
    font-family: 'Europa Light', Helvetica, Arial, sans-serif;
  }

  #app {
    background-color: #fafafa;
    min-height: 100%;
    min-width: 100%;
  }

  p,
  label {
    font-family: Georgia, Times, 'Times New Roman', serif;
    line-height: 1.5em;
  }
`;

export default GlobalStyle;
